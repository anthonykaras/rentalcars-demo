// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

var locationSearch = {
  init: function() {
    locationSearch.searchLocation();
  },

  searchLocation: function() {
    $('.locationSearch').on('keyup', function() {
      var term = $('.locationSearch').val();
      if (term.length <= 1) {
        $('.resultsList').html('');
        return false;
      }
      var url = 'https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm=' + term;
      $.ajax({
        url: url,
        success: function(data) {
          console.log('data', data);
          const jsonData = $.parseJSON(data);
          locationSearch.parseResults(jsonData);
        }
      });
    });
  },

  parseResults: function(data) {
    var results = data.results.docs;
    var lis = $.map(results, function(result, index) {
      return locationSearch.buildLi(result);
    });
    $('.resultsList').html(lis);
  },

  buildLi: function(result) {
    if (result.name === 'No results found') {
      return '<li class="resultListItem">' + result.name + '</li>';
    }
    var countryCity = [result.city, result.country];
    countryCity = countryCity.filter(function(n) { return typeof n !== 'undefined' });
    countryCity = countryCity.join(', ');
    return '<li class="resultListItem">' + result.name + ' (' + countryCity + ')</li>';
  }
};
