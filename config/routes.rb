Rails.application.routes.draw do
  resources :rental_cars
  root to: 'rental_cars#index'
end
